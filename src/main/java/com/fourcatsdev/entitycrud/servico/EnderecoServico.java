package com.fourcatsdev.entitycrud.servico;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fourcatsdev.entitycrud.modelo.Endereco;
import com.fourcatsdev.entitycrud.repositorio.EnderecoRepositorio;

@Service
public class EnderecoServico {
	
	@Autowired
	private EnderecoRepositorio enderecoRepositorio;
	
	public Endereco salvar (Endereco endereco) {
		return enderecoRepositorio.save(endereco);
	}

}
