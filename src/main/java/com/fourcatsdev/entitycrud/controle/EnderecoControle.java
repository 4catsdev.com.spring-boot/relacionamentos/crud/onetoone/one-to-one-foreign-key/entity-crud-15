package com.fourcatsdev.entitycrud.controle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fourcatsdev.entitycrud.excecao.EstudanteNotFoundException;
import com.fourcatsdev.entitycrud.modelo.Endereco;
import com.fourcatsdev.entitycrud.modelo.Estudante;
import com.fourcatsdev.entitycrud.servico.EnderecoServico;
import com.fourcatsdev.entitycrud.servico.EstudanteServico;

import jakarta.validation.Valid;


@Controller
@RequestMapping("/endereco")
public class EnderecoControle {

	@Autowired
	private EnderecoServico enderecoServico;
	
	@Autowired
	private EstudanteServico estudanteServico;
	
	@GetMapping("/buscar-endereco/{id}")
	public String novoEndereco(@PathVariable("id") long id, Model model) {
		String pagina = "";
		try {
			Estudante estudante = estudanteServico.buscarEstudantePorId(id);
			if(estudante.getEndereco() == null) {
				Endereco endereco = new Endereco();
				endereco.setEstudante(estudante);
				model.addAttribute("item", endereco);
				pagina = "/novo-endereco";
			} else {
				pagina = "redirect:/";
			}
		} catch (EstudanteNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return pagina;
	}
	
	@PostMapping("/gravar-endereco/{idEstudante}")
	public String gravarEndereco(@PathVariable("idEstudante") long idEstudante,
			@ModelAttribute("item") @Valid Endereco endereco, BindingResult result, 
			RedirectAttributes attributes) {
		
		try {
			Estudante estudante = estudanteServico.buscarEstudantePorId(idEstudante);
			endereco.setEstudante(estudante);
		} catch (EstudanteNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(result.hasErrors()) {
			return "/novo-endereco";
		}
		enderecoServico.salvar(endereco);
		attributes.addFlashAttribute("mensagem", "Endereçoe salvo com sucesso!");
        return "redirect:/endereco/buscar-endereco/"+idEstudante;
	}
		
}
